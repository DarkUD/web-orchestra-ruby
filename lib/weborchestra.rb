require "weborchestra/version"
require "watir-webdriver"
require "headless"

require "colorize"
require "optparse"
require "json"

require "processors"


module Weborchestra
  class Nexus

    def init

      options = {}

      optParser = OptionParser.new do|opts|

        opts.banner = "Usage: weborchestra [options] file"

        #Define if the webdriver test should be headless or not
        options[:screen] = false
        opts.on("-s","--screen","Show the result on an actual browser") do
          options[:screen] = true
        end

        opts.on('-h','--help',"Display this screen") do
          puts opts
          exit
        end
      end

      # We parse the current options
      optParser.parse!

      @options = options;

      ARGV.each do |f|

        begin
          file = File.read f
          if file
            params = parseWO file
            if params
              processor_name = params['processor']
              startProcessor processor_name, params
            end
          end
        rescue StandardError => e
          puts "x> Error Processing file #{f} : #{e.message}".colorize(:red)
        end

      end
    end

    #We exiting cleanly
    def end
      puts "Exiting ..."
    end

    #We parse the entry files
    def parseWO config
      begin
        params = JSON.parse config
      rescue
        puts "x> Error while parsing the config file".colorize(:red)
      end
    end

    #we start the webdriver
    def startWD
      #@webdriver = Watir::Browser.new
      if(!@options[:screen])
        @headless = Headless.new
        @headless.start
      end
    end

    # we end the webdriver
    def endWD
      #@webdriver.close
      if(!@options[:screen])
        @headless.destroy
      end
    end

    #we restart the webdriver
    def restartWD
      endWD
      startWD
    end

    #we start the correct processor by its name
    def startProcessor name,params

      case name
      when "adwords"
        startWD
        @processor = WoProcessor::AdwordsProcessor.new name,params,@webdriver
        @processor.init
        endWD
      end
    end

    #we end the instanciated processor
    def endProcessor
      @processor.end
    end

  end
end
