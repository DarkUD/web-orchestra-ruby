require 'fileutils'
require "colorize"
require "watir-webdriver"

module WoProcessor

  class Processor
    attr_reader :name
    attr_accessor :status

    def initialize name = "default",params = {}, webdriver
      @name = name
      @params = params;
      @test_params = params["params"]
      @webdriver = webdriver
    end

    def start
      log "starting Processor"
    end

    def stop
      log "ending Processor"
    end

    def log msg = ""
      puts "o> #{msg}".colorize(:yellow)
    end

    def err msg = ""
      puts "x> #{msg}".colorize(:red)
    end

  end

  class AdwordsProcessor < Processor
    BATCH_VOL = 800

    def err msg=""
      super
      ss __method__.to_s + ".png"
    end

    def init
      # Create folder structure
      if @params['name']
        @test_name = @params['name']
      else
        @test_name = "default-test"
      end

      @testDir = @test_name
      @dlDir = @test_name + "/downloads"
      @ssDir = @test_name + "/screenshots"

      Dir.mkdir @test_name unless Dir.exists? @test_name
      Dir.mkdir @dlDir unless Dir.exists? @dlDir
      Dir.mkdir @ssDir unless Dir.exists? @ssDir

      # We save the params in the test instance
      @login = @test_params["login"]
      @password = @test_params["password"]
      @keywords = @test_params["keywords"]
      @countries = @test_params["countries"]
      @languages = @test_params["languages"]

      # We prepare the firefox profile so the download go smoothly
      @download_directory = "#{Dir.pwd}/#{@test_name}/downloads"
      @download_directory.gsub!("/", "\\") if Selenium::WebDriver::Platform.windows?

      profile = Selenium::WebDriver::Firefox::Profile.new
      profile['browser.download.folderList'] = 2
      profile['browser.download.dir'] = @download_directory
      profile['browser.helperApps.neverAsk.saveToDisk'] = "application/x-csv,text/csv,application/pdf"

      @webdriver = Watir::Browser.new :firefox, :profile => profile
      dir_before = Dir.entries(@download_directory)

      start
      stop
    end

    def start
      super

      # step_connect
      # step_insert_keywords

      begin
        step_connect
        step_insert_keywords
      rescue StandardError => e
        err "An error occured : " + e.message.to_s
      end

    end

    def step_connect
      log "Connecting using #{@login}:#{@password}"
      @webdriver.goto 'www.adwords.google.com?hl=en'

      email = @webdriver.text_field :id => 'Email'
      email.set(@login)

      passw = @webdriver.text_field :id => 'Passwd'
      passw.set(@password)

      @webdriver.send_keys :enter

      sleep 10
      Watir::Wait.until(60){ @webdriver.text.include? 'Customer ID' }

      if @webdriver.text.include? 'Customer ID'
        log "Connected to the adwords account"
      else
        err "Something went wrong while connecting"
      end

      ss __method__.to_s + ".png"
    end

    def step_insert_keywords
      # if the keywords list is not an array we convert it in that
      if !@keywords.is_a? Array
        @keywords = @keywords.to_s.split(',')
      end

      # We access the test page for adwords
      # Now we need to processs the keywords by batches of 800
      nb_keywords = @keywords.length

      nb_batches = nb_keywords.to_f / BATCH_VOL
      nb_batches = nb_batches.ceil

      processed_batches = 0;

      for batch in 0...nb_batches
        if processed_batches == nb_batches
          break
        end

        log "Accessing the keywordPlanner"

        @webdriver.goto "https://adwords.google.com/ko/KeywordPlanner/Home"
        sleep 5

        label = @webdriver.div :class => /gwt-Label/, :text => "Get search volume for a list of keywords or group them into ad groups"

        if label.exists?
          label.click
        else
          err "Can't find the label to click"
        end

        batch_keywords = @keywords[batch*BATCH_VOL,BATCH_VOL]
        log "Processing #{batch_keywords.length} keywords"
        batch_keywords = batch_keywords * "\n"

        textarea = @webdriver.textarea :class => /gwt-TextArea/
        textarea.set batch_keywords

        ss __method__.to_s + "_batch-#{batch}-#{nb_batches}.png"

        buttons = @webdriver.elements :class => "goog-button-base-content", :text => "Get search volume"
        buttons.each do |button|
          button.click if button.visible?
        end

        sleep 5
        select_locations
        select_languages
        sleep 5

        #We download the results now
        download_results
      end

    end

    # Inject the locations on the keyword planner
    def select_locations
      log "Inserting locations : " + @countries.to_s

      countries = @countries.to_s.split ","

      buttons = @webdriver.divs :css=> "[aria-label='Edit location targeting'] div[role='button']"

      # We open the locations selector
      buttons.each do |button|
        button.click if button.visible?
      end

      sleep 1

      # We click on remove all button
      remove_all = @webdriver.link :text => /Remove all/
      if remove_all.visible?
        remove_all.click
      else
        throw "Couldn't find the remove all button"
      end

      # We add the countries

      entry = @webdriver.text_field :aria_label => "Enter a location to target.", :class => "gwt-SuggestBox"
      countries.each do |country|
        entry.set country.strip
        @webdriver.send_keys :enter
        sleep 1
      end

      ss "locations_inserted"

      @webdriver.body.click
    end

    # Select a language given in parameter
    def select_languages
      log "Inserting languages : " + @languages.to_s

      languages = @languages.to_s.split ","

      buttons = @webdriver.divs :css=> "[aria-label='Edit language targeting'] div[role='button']"

      # We click on the language selector
      buttons.each do |button|
        button.click if button.visible?
      end

      # We remove all the languages if any
      remove_languages = @webdriver.divs :id=>"gwt-debug-popup-pill-remove-button"

      remove_languages.each do |button|
        button.click if button.visible?
      end

      # We add a language if it exists
      entry = @webdriver.text_field :id => "gwt-debug-language-pill-input-box"
      languages.each do |language|
        entry.set language.strip
        @webdriver.send_keys :enter
        sleep 1
      end

      ss "languages_inserted"

      @webdriver.body.click
    end

    # We download the results
    def download_results
      languages = @languages.to_s.split ","

      if languages.count == 0
        # We selected All languages
        ss "results-all-languages"
        log "Downloading results for all languages"

        dir_before = Dir.entries(@download_directory)
        download_csv

        file_name = nil;

        difference = Dir.entries(@download_directory) - dir_before

        if difference.size == 1
          file_name = difference.first
        end

        File.rename(@download_directory + "/#{file_name}",@download_directory + "/result-all.csv") if file_name

        @webdriver.body.click
      else

        languages.each do |language|
          tabs = @webdriver.divs :css => "[role='tab'] .gwt-Label"

          tabs.each do |tab|

            if tab.text == language
              log "Downloading results for " + language
              tab.click

              dir_before = Dir.entries(@download_directory)
              download_csv

              file_name = nil;

              difference = Dir.entries(@download_directory) - dir_before

              if difference.size == 1
                file_name = difference.first
              end

              File.rename(@download_directory + "/#{file_name}",@download_directory + "/result-#{language}.csv") if file_name

              @webdriver.body.click
            end

          end

        end
      end

    end

    # We download the csv
    def download_csv
      dl_buttons = @webdriver.divs  :class => "goog-button-base-content", :text => "Download"

      dl_buttons.each do |button|
        button.click if button.visible?
      end

      sleep 5

      download = @webdriver.span :id => "gwt-debug-download-button-content"

      if download.visible?
        download.click
      else
        err "Counldn't find the download button"
      end

      sleep 5

      save_file = @webdriver.span :id => "gwt-debug-retrieve-download-content"
      if save_file.visible?
        save_file.click
      else
        err "Counldn't find the Save to button"
      end

      sleep 15
    end

    def stop
      super
      @webdriver.close
    end

    def ss name = "screenshot.png"
      if name == "screenshot.png"
        name = "screenshot_"+ (Time.now.getutc).to_s + ".png"
      end
      @webdriver.screenshot.save @ssDir + "/#{name}"
    end
  end

end
