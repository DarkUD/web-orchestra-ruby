# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'weborchestra/version'

Gem::Specification.new do |spec|
  spec.name          = "weborchestra"
  spec.version       = Weborchestra::VERSION
  spec.authors       = ["gb"]
  spec.email         = ["g.boris@gmail.com"]
  spec.summary       = 'Web Orchestra'
  spec.description   = 'Web Orchestra'
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = ["weborchestra"]
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "json"
  spec.add_development_dependency "colorize"
  spec.add_development_dependency "watir-webdriver"
  spec.add_development_dependency "headless"
end
